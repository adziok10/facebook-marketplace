const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const path = require('path');

const keys = require('./config/keys-config');
const authRouter = require('./routes/auth-routes');
const apiFanpagesRouter = require('./routes/api-fanpages-routes');
const apiRouter = require('./routes/api-routes');
const facbookService = require('./services/facbook-service');

mongoose.connect(keys.mongo.url, (err) => {
    if(!err)    
        console.log('---> Connect to mongo db');
    else{
        console.log('---> Can`t connect to mongo db');
        process.exit(1);
    }
});

const app = express();

app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({extended: true,limit: '50mb'}))

app.use(cookieSession({
    maxAge: keys.session.maxAge,
    keys: [keys.session.cookieKey]
}));


app.use(passport.initialize());
app.use(passport.session());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use('/auth', authRouter);
app.use('/api/fanpages', apiFanpagesRouter);
app.use('/api', apiRouter);

app.use('/app', express.static(__dirname + '/web/dist'));

app.use('*', (req,res) => {  
    res.sendFile(path.resolve('web/dist/index.html'));
});

app.listen(keys.express.port, () => console.log( `---> Facebook-marketplace app listening on port ${keys.express.port}!`))