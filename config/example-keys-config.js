module.exports = {
    facebook: {
        clientID: 123,
        clientSecret: "abc",
        callbackURL: "url"
    },
    express:{
        port: process.env.PORT || 3000
    },
    mongo:{
        url:'database url'
    },
    session:{
        cookieKey: 'abc',
        maxAge: 24*60*60*1000
    }
};