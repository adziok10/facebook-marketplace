const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const axios = require('axios');

const UserFanpage = require('./user-fanpages-model');

const fanpageSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    name: String,
    fanpageID: String,
    picture: {
        type: String,
        default: "https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg"
    },
    forSale: Boolean,
    price: Number,
    fanCount: String,
    description: String,
    createDate: Date,
    updateDate: Date,

});

fanpageSchema.pre('save', (next) => {
    if (!this.createDate) {
        this.createDate = new Date();
    }
    this.updateDate = new Date();

    next();
});

const Fanpage = mongoose.model('fanpage', fanpageSchema);

Fanpage.getFanpages = () => {
    return new Promise((resolve, reject) => {
        Fanpage.find().then((fanpages) => {
            if(!fanpages)
                reject("No fanpages found");
            let fanpagesArray = Array();
            console.log(fanpages)
            fanpages.map((fanpage) => {
                fanpagesArray.push({
                    picture: fanpage.picture,
                    user: fanpage.user,
                    name: fanpage.name,
                    fanpageID: fanpage.fanpageID,
                    forSale: fanpage.forSale,
                    price: fanpage.picture,
                    description: fanpage.price,
                });
            });
            
            resolve(fanpagesArray);
        });
    });
};

Fanpage.addNewFanpage = (req) => {
    if (UserFanpage.isUserFanpage(req)) {
        Fanpage.findOne({
            fanpageID: req.body.id
        }).then((currentFanpage) => {
            if (currentFanpage) {
                console.log('fanpage already exists');
            } else {
                axios.get('https://graph.facebook.com/v3.2/' + req.body.id + '?access_token=' + req.user.accessToken + '&fields=name,picture,cover,fan_count,about,description&format=json&limit=30000&method=get&pretty=0&suppress_http_code=1')
                    .then(function (response) {
                        new Fanpage({
                            user: req.user._id,
                            name: response.data.name,
                            fanpageID: response.data.id,
                            picture: response.data.picture.data.url,
                            forSale: true,
                            fanCount: response.data.fan_count,
                            price: req.body.price,
                            description: req.body.description,
                        }).save().then((newFanpage) => {
                            console.info("new fanpage add");
                        });
                    }).catch(function (error) {
                        console.log('error');
                    });
            }
        });
    } else {
        console.log('no promisions');
    }
};

Fanpage.getFanpageByID = (id) => {
    return new Promise((resolve, reject) => {
        Fanpage.findOne({
            fanpageID: id
        }).then((currentFanpage) => {
            let response;
            if (!currentFanpage) {
                response = {error: "Not found"};
            } else {
                response = currentFanpage.toObject();
            }
            delete response.__v;
            delete response._id;
            delete response.user;
            resolve(response);
        });
    });
};

Fanpage.deleteFanpageByID = (req) => {
    if (UserFanpage.isUserFanpage(req)) {
        Fanpage.findOneAndRemove({ fanpageID: req.params.id}).then((res) => {
            console.log(res)
        });
        return {message: "Succeful delete fanpage"}
    }
    return {error: "No promision"};
};

module.exports = Fanpage;