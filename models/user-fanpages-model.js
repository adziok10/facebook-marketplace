const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const axios = require('axios');

const userFanpageSchema = new Schema({
    user: String,
    name: String,
    fanpageID: String
});

const UserFanpage = mongoose.model('userFanpage', userFanpageSchema);

UserFanpage.getUserFanpages = (req) => {
    return new Promise((resolve, reject) => {
        UserFanpage.find({
            user: req.user.facebookID
        }).then((fanpages) => {
            if(!fanpages)
                reject("No fanpages found");
            let fanpagesArray = Array();
            fanpages.map((fanpage) => {
                fanpagesArray.push({
                    name: fanpage.name,
                    id: fanpage.fanpageID
                });
            });
            resolve(fanpagesArray);
        });
    });
};

UserFanpage.createOrUpdateList = ((accessToken, userID) => {
    userFanpagesArray = Array();
    axios.get('https://graph.facebook.com/v3.2/me/accounts?access_token=' + accessToken)
        .then((response) => {
            response.data.data.map((page) => {
                userFanpagesArray.push({
                    user: userID,
                    name: page.name,
                    fanpageID: page.id
                });
            });
            // console.info(userFanpagesArray);
            userFanpagesArray.map((page) => {
                UserFanpage.findOne({
                    fanpageID: page.fanpageID
                }).then((Currentfanpage) => {
                    if (Currentfanpage) {
                        return true;
                    } else {
                        new UserFanpage(page).save().then((newFanpage) => {
                            return true;
                        });
                    }
                });
            });
        }).catch((error) => {
            console.log('error');
            return false;
        });
});

UserFanpage.isUserFanpage = (req) => {
    UserFanpage.getUserFanpages(req).then((userFanpagesArray) => {
        if(userFanpagesArray){
        userFanpagesArray.map((fanpage) => {
            if(fanpage.fanpageID == req.body.id)
                return true;
        });
    }
    });
    return false;
};

module.exports = UserFanpage;