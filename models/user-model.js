const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    lastName: String,
    firstName: String,
    email: String,
    facebookID: String,
    accessToken: String,
    createDate: Date,
    updateDate: Date,
});

userSchema.pre('save', (next) => {
    if(!this.createDate) {
        this.createDate = new Date();
    }
    this.updateDate = new Date();

    next();
});

const User = mongoose.model('user',userSchema);

module.exports = User;