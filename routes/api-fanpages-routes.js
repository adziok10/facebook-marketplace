const router = require('express').Router();
const axios = require('axios');

const authCheck = require('../helpers/auth-helper').authCheck;
const UserFanpage = require('../models/user-fanpages-model');
const Fanpage = require('../models/fanpage-model');



router.get('/', (req, res) => {
    if(req.query.user_pages !== undefined && req.query.user_pages == 1){
        if(req.user){
            UserFanpage.getUserFanpages(req).then((data) => {
                res.status(200).json(data);
            });
        } else {
            res.status(403).json({eroor: 'log in'});
        }
    } else {
        Fanpage.getFanpages().then((data) => {
            res.status(200).json(data);
        });
    }
});

router.post('/', authCheck, (req, res) => {
    Fanpage.addNewFanpage(req);
    res.status(201);
});

router.get('/:id', (req, res) => {
    Fanpage.getFanpageByID(req.params.id).then((response) => {
        if(response.error){
            res.status(404).json(response);
        } else {
            res.status(200).json(response);
        }
    });
});

router.delete('/:id', authCheck, (req, res) => {
    Fanpage.deleteFanpageByID(req);
});


module.exports = router;

