const router = require('express').Router();
const authCheck = require('../helpers/auth-helper').authCheck;


router.get('/', (req, res) => {
    res.json({message:'work'});
});

router.get('/auth_test', authCheck, (req, res) => {
    res.json({auth:true});
});

module.exports = router;

