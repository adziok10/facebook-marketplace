const router = require('express').Router();
const passport = require('passport');

router.get('/facebook', passport.authenticate('facebook'));

router.get('/facebook/cb', passport.authenticate('facebook', { successRedirect: '/', failureRedirect: '/facebook/fail' }));

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

router.get('/facebook/fail', (req, res) => {
    res.json({error: 'Facebook authorizatiob problem'});
});

module.exports = router;