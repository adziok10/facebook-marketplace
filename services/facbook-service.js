const passport = require('passport');
const FacebookStrategy = require('passport-facebook');

const User = require('../models/user-model');
const UserFanpage = require('../models/user-fanpages-model');
const keys = require('../config/keys-config');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user);
    })
});

passport.use(new FacebookStrategy({
    clientID: keys.facebook.clientID,
    clientSecret: keys.facebook.clientSecret,
    callbackURL: keys.facebook.callbackURL,
    profileFields: ['id', 'emails', 'name']
}, (accessToken, refreshToken, profile, done) => {
    User.findOne({
        facebookID: profile.id
    }).then((currentUser) => {
        if (currentUser) {
            UserFanpage.createOrUpdateList(accessToken, profile.id);
            done(null, currentUser);
        } else {
            new User({
                lastName: profile._json.last_name,
                firstName: profile._json.first_name,
                email: profile._json.email,
                facebookID: profile._json.id,
                accessToken: accessToken
            }).save().then((newUser) => {
                UserFanpage.createOrUpdateList(accessToken, profile.id);
                done(null, newUser);
            });
        }
    });
}));