{
    name: 'Webowisko',
    picture: {
        data: {
            height: 50,
            is_silhouette: false,
            url: 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/37209128_365042594023818_7395697353136537600_n.png?_nc_cat=104&_nc_ht=scontent.xx&oh=cb1b6a12fca99845524566a0d41069d7&oe=5CA30AF6',
            width: 50
        }
    },
    cover: {
        cover_id: '370731506788260',
        offset_x: 49,
        offset_y: 50,
        source: 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/37655403_370731513454926_8929510417412128768_o.jpg?_nc_cat=100&_nc_ht=scontent.xx&oh=3e75b2b393c917ca8feb719d311a4e0f&oe=5C9A0ED0',
        id: '370731506788260'
    },
    fan_count: 1,
    about: 'Webowisko to firma, w której kompleksowo dbamy o Twój wizerunek w sieci. Nowoczesne, szybkie i responsywne strony internetowe, kreatywne prowadzenie profili w social media jak Facebook i Instagram oraz perswazyjne teksty sprzedażowe na Twoją stronę w',
    id: '365042454023832'
}