import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterModule, CanActivateChild } from '@angular/router';

import { HomeComponent } from './core/home/home.component';
import { FanpagesListComponent } from './fanpages/fanpages-list/fanpages-list.component';
import { FanpagesItemComponent } from './fanpages/fanpages-item/fanpages-item.component';
import { FanpagesUserListComponent } from './fanpages/fanpages-user-list/fanpages-user-list.component';
import { AuthGuard } from './shared/auth-guard.service';
import { FanpagesUserEditComponent } from './fanpages/fanpages-user-edit/fanpages-user-edit.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'fanpages', component: FanpagesListComponent },
  { path: 'fanpages/user', component: FanpagesUserListComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    children: [
      { path: ':id', component: FanpagesUserEditComponent},
    ]
  },
  { path: 'fanpages/:id', component: FanpagesItemComponent}
  // { path: 'recipes', loadChildren: './recipes/recipes.module#RecipesModule'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes) // , {preloadingStrategy: PreloadAllModules}) add when lazy load modules is implement
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
