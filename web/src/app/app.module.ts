import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { FanpagesItemComponent } from './fanpages/fanpages-item/fanpages-item.component';
import { FanpagesListComponent } from './fanpages/fanpages-list/fanpages-list.component';
import { FanpagesUserEditComponent } from './fanpages/fanpages-user-edit/fanpages-user-edit.component';
import { FanpagesUserListComponent } from './fanpages/fanpages-user-list/fanpages-user-list.component';
import { ApiService } from './shared/api.service';
import { AuthGuard } from './shared/auth-guard.service';
import { AuthInterceptor } from './shared/auth.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    FanpagesListComponent,
    FanpagesItemComponent,
    FanpagesUserListComponent,
    FanpagesUserEditComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    CoreModule,
  ],
  providers: [
    ApiService,
    AuthGuard,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {  }
