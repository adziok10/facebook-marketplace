import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { AuthComponent } from './header/auth/auth.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { NavComponent } from './header/nav/nav.component';

@NgModule({
    declarations: [
        HeaderComponent,
        AuthComponent,
        HomeComponent,
        NavComponent,
    ],
    imports: [
        CommonModule,
        RouterModule
    ],
    exports: [
        HeaderComponent
    ]
})
export class CoreModule { }
