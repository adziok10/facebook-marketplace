import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/api.service';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit, OnDestroy {
  isLoggedIn = false;
  private authSubject: Subject<boolean>;
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.authSubject = this.api.checkIsLoggidIn();
    this.authSubject.subscribe((d) => {
        this.isLoggedIn = d;
    });
  }

  ngOnDestroy() {
    this.authSubject.unsubscribe();
  }

}
