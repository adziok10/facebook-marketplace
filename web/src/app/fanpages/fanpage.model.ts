export class Fanpage {
    picture: string;
    name: string;
    fanpageID: string;
    forSale: boolean;
    price: number;
    description: string;
}
