import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'app-fanpages-item',
  templateUrl: './fanpages-item.component.html',
  styleUrls: ['./fanpages-item.component.sass']
})
export class FanpagesItemComponent implements OnInit {

  id: number;
  fanpage;
  isExists = false;

  constructor(private api: ApiService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.api.getFanpageItem(this.id).then((fanpage) => {
            this.fanpage = fanpage;
            this.isExists = true;
          });
        }
      );
  }

}
