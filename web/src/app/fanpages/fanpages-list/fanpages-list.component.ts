import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Fanpage } from '../fanpage.model';

@Component({
  selector: 'app-fanpages-list',
  templateUrl: './fanpages-list.component.html',
  styleUrls: ['./fanpages-list.component.sass']
})
export class FanpagesListComponent implements OnInit {
  fanpagesList: Fanpage[];

  constructor( private api: ApiService ) { }

  ngOnInit() {
    this.api.getFanpagesList().subscribe((data) => {
      this.fanpagesList = data;
    });
  }

}
