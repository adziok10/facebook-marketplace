import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiService } from 'src/app/shared/api.service';
import { Fanpage } from '../fanpage.model';
import { UserFanpage } from '../user-fanpage.model';


@Component({
  selector: 'app-fanpages-user-edit',
  templateUrl: './fanpages-user-edit.component.html',
  styleUrls: ['./fanpages-user-edit.component.sass']
})
export class FanpagesUserEditComponent implements OnInit, OnDestroy {

  private id;
  private fanpageName;
  fanpage: Fanpage;
  isExists: boolean;

 

  constructor(private api: ApiService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.api.getFanpageItem(this.id).then((fanpage: Fanpage) => {
            if (fanpage) {
              this.fanpage = fanpage;
              this.isExists = true;
            } else {
              this.isExists = false;
            }
          });
        }
      );
    if(!this.isExists){
      this.route.params
        .subscribe(
          (params: Params) => {
            this.id = +params['id'];
            this.api.getUserFanpagesList().subscribe((fanpage: UserFanpage[]) => {
              if (fanpage) {
                const h = fanpage.find( el => el.id === this.id );
                this.fanpageName = h.name;
              }
            });
          }
        );
    }
  }

  ngOnDestroy() {
    this.fanpage = null;
  }


}
