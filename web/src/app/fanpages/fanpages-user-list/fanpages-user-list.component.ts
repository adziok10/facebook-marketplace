import { Component, OnInit } from '@angular/core';

import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-fanpages-user-list',
  templateUrl: './fanpages-user-list.component.html',
  styleUrls: ['./fanpages-user-list.component.sass']
})
export class FanpagesUserListComponent implements OnInit {

  fanpagesList: any;

  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getUserFanpagesList().subscribe((data) => {
      this.fanpagesList = data;
    });
  }

}
