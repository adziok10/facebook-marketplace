import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, interval } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { UserFanpage } from '../fanpages/user-fanpage.model';
import { Fanpage } from '../fanpages/fanpage.model';

@Injectable()
export class ApiService {
    isLoggedIn = false;
    link = 'http://localhost:3000/';

    private isLoggedInSubject = new Subject<boolean>();
    fanpagesList = [
        {
            picture: 'https://scontent.xx.fbcdn.net/v/t1.0-1/c15.0.50.50a/p50x50/399548_10149999285987789_1102888142_n.png?_nc_cat=1&_nc_ht=scontent.xx&oh=c78833970191bdc0a1a78d3b4dca4651&oe=5CAA9E05',
            name: 'Rowerowe melodie',
            fanpageID: '1046422925390283',
            forSale: true,
            price: 1600,
            description: 'FAjna strona fest'
        },
        {
            picture: 'https://scontent.xx.fbcdn.net/v/t1.0-1/c15.0.50.50a/p50x50/399548_10149999285987789_1102888142_n.png?_nc_cat=1&_nc_ht=scontent.xx&oh=c78833970191bdc0a1a78d3b4dca4651&oe=5CAA9E05',
            name: 'Rowerowe melodie 2',
            fanpageID: '1046422925390286',
            forSale: true,
            price: 1600,
            description: 'FAjna strona fest'
        },
        {
            picture: 'https://scontent.xx.fbcdn.net/v/t1.0-1/c15.0.50.50a/p50x50/399548_10149999285987789_1102888142_n.png?_nc_cat=1&_nc_ht=scontent.xx&oh=c78833970191bdc0a1a78d3b4dca4651&oe=5CAA9E05',
            name: 'Rowerowe melodie 3',
            fanpageID: '1046422925390287',
            forSale: true,
            price: 1600,
            description: 'FAjna strona fest'
        },
        {
            picture: 'https://scontent.xx.fbcdn.net/v/t1.0-1/c15.0.50.50a/p50x50/399548_10149999285987789_1102888142_n.png?_nc_cat=1&_nc_ht=scontent.xx&oh=c78833970191bdc0a1a78d3b4dca4651&oe=5CAA9E05',
            name: 'Rowerowe melodie 4',
            fanpageID: '1046422925390288',
            forSale: true,
            price: 1600,
            description: 'FAjna strona fest'
        },
    ];

    constructor(private http: HttpClient) {
        const obser = interval(5000);
        obser.subscribe((n) => {
            console.log(n);
            this.checkIsLoggidIn();
        });
     }

    OnInit() {
    }

    checkIsLoggidIn(): Subject<boolean> {
        this.http.get(this.link + 'api/auth_test').subscribe(
            (data) => {
                this.isLoggedIn = data ? true : false;
                this.isLoggedInSubject.next( this.isLoggedIn );
            }, (error) => {
                this.isLoggedInSubject.next( false );
            }
        );

        return this.isLoggedInSubject;
    }

    getFanpagesList(): Observable<Fanpage[]> { // TODO add option to laod fanpages from passed parametr
        return this.http.get<Fanpage[]>(this.link + 'api/fanpages');
    }

    getFanpageItem(id: number) {
        return new Promise((resolve, reject) => {
            resolve(this.fanpagesList.find((fanpage) => {
                return +fanpage.fanpageID === id;
            }));
        });
    }

    getUserFanpagesList(): Observable<UserFanpage[]> {
        return this.http.get<UserFanpage[]>(this.link + 'api/fanpages?user_pages=1');
    }

}
