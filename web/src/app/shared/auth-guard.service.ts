import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ApiService } from './api.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, OnDestroy {

  private isLoggedIn = false;
  private authSubject: Subject<boolean>;

  constructor(private api: ApiService) {
    this.authSubject = this.api.checkIsLoggidIn();
    this.authSubject.asObservable().subscribe((d) => {
        console.log(d);
        this.isLoggedIn = d;
    });
  }

  ngOnDestroy() {
    this.authSubject.unsubscribe();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable < boolean > | Promise < boolean > | boolean {
    console.log(this.isLoggedIn);
    return this.isLoggedIn;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable < boolean > | Promise < boolean > | boolean {
    return this.isLoggedIn;
  }

}
